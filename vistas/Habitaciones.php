<?php
  /* Listar Hoteles */
  $datos = json_decode(file_get_contents("http://hotelesttt.webcindario.com/rest/ApiHoteles.php"),true);
  /* Listar habitaciones */
  $tabla = json_decode(file_get_contents("http://hotelesttt.webcindario.com/rest/ApiHabitaciones.php"),true);
?>
<html>
<head>
	<title></title>
</head>
<body>
  <section id="projects" class="projects-section bg-light">
    <div class="container">

      <div class="row align-items-center no-gutters mb-4 mb-lg-5">
        <div class="col-xl-8 col-lg-7">
          <table class="table table-responsive">
              <thead>
                  <tr>
                      <th>Nombre hotel</th>
                      <th>Total habitaciones</th>
                      <th>Cantidad</th>
                      <th>Tipo habitación</th>
                      <th>Acomodación</th>
                  </tr>
              </thead>
              <tbody>
                <?php for ($pila=0; $pila < count($tabla); $pila++) { ?>
                  <tr>
                      <td><?php echo $tabla[$pila]["Nombre"]; ?></td>
                      <td><?php echo $tabla[$pila]["NumeroHabitaciones"]; ?></td>
                      <td><?php echo $tabla[$pila]["Cantidad"]; ?></td>
                      <td><?php echo $tabla[$pila]["TipoHabitacion"]; ?></td>
                      <td><?php echo $tabla[$pila]["Acomodacion"]; ?></td>
                  </tr>
                  <?php }  ?>
              </tbody>
          </table><br><br><br><br><br><br><br><br><br>
        </div>
        <div class="col-xl-4 col-lg-5">
          <div class="featured-text text-center text-lg-left">
            <h4>Gestión de habitaciones</h4>
            <p class="text-black-50 mb-0">
              <form class="form-inline d-flex" method="post" >
                <select class="form-control flex-fill mr-0 mr-sm-2 mb-3 mb-sm-0" name="IdHotel" required="">
                  <option value="">Elija una opción</option>
                  <?php for ($contador=0; $contador < count($datos); $contador++) { ?>
                   <option value="<?php echo $datos[$contador]['IdHotel']; ?>" ><?php echo $datos[$contador]['Nombre']; ?></option>
                  <?php } ?>
                </select>                
                <input type="text" class="form-control flex-fill mr-0 mr-sm-2 mb-3 mb-sm-0" name="Cantidad" placeholder="Cantidad" required="">
                <select class="form-control flex-fill mr-0 mr-sm-2 mb-3 mb-sm-0" name="TipoHabitacion" required="">
                  <option value="">Elija una opción</option>
                  <option>Estándar</option>
                  <option>Junior</option>
                  <option>Suite</option>
                </select>
                <select class="form-control flex-fill mr-0 mr-sm-2 mb-3 mb-sm-0" name="Acomodacion" required="">
                  <option value="">Elija una opción</option>
                  <option>Sencilla</option>
                  <option>Doble</option>
                  <option>Triple</option>
                  <option>Cuádruple</option>
                </select>   
                <input type="hidden" name="UsuarioSesion" value="<?php echo $session['Usuario']; ?>"> 
                <input type="hidden" name="NombreSesion" value="<?php echo $session['Nombre']; ?>">                   
                <input type="submit" name="HabitacionesGuardar" class=" btn-primary mx-auto " value="Guardar">
            </form>
            </p>
          </div>
        </div>
      </div>

    </div>
  </section>

</body>
</html>