<?php 
/* Listar Hoteles */
$datos = json_decode(file_get_contents("http://hotelesttt.webcindario.com/rest/ApiHoteles.php"),true);
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>

<body>
 <section id="about" class="about-section text-center">
  	<div class="container">
	  	<div class="row">
			<div class="col-lg-6 mx-0">
				<h2 class="text-white mb-4">Gestión de hoteles</h2>
		          <p class="text-white-50" align="left">
		          	<form class="form-inline d-flex" method="post" >
				        <input type="text" class="form-control flex-fill mr-0 mr-sm-2 mb-3 mb-sm-0" name="Nombre" placeholder="Nombre" required="">
				        <input type="text" class="form-control flex-fill mr-0 mr-sm-2 mb-3 mb-sm-0" name="Ciudad" placeholder="Ciudad" required="">
				        <input type="text" class="form-control flex-fill mr-0 mr-sm-2 mb-3 mb-sm-0" name="Numero" placeholder="Número de habitaciones" required="">
				        <input type="text" class="form-control flex-fill mr-0 mr-sm-2 mb-3 mb-sm-0" name="Direccion" placeholder="Dirección" required="">
				        <input type="text" class="form-control flex-fill mr-0 mr-sm-2 mb-3 mb-sm-0" name="Nit" placeholder="Nit" required="">
				        <input type="hidden" name="UsuarioSesion" value="<?php echo $session['Usuario']; ?>"> 
                		<input type="hidden" name="NombreSesion" value="<?php echo $session['Nombre']; ?>">				        
				        <input type="submit" name="HotelGuardar" class=" btn-primary mx-auto " value="Guardar">
	    			</form>
		          </p>
	        </div>
	        <div class="col-lg-6 mx-auto">
	        	<table class="table table-responsive text-white">
				    <thead>
				        <tr>
				            <th>Nombre</th>
				            <th>Ciudad</th>
				            <th>Numero de habitaciones</th>
				            <th>Dirección</th>
				            <th>Nit</th>
				        </tr>
				    </thead>
				    <tbody>
				    	<?php for ($contador=0; $contador < count($datos) ; $contador++) { ?>
				        <tr>
				            <td><?php echo $datos[$contador]["Nombre"]; ?></td>
				            <td><?php echo $datos[$contador]["Ciudad"]; ?></td>
				            <td><?php echo $datos[$contador]["NumeroHabitaciones"]; ?></td>
				            <td><?php echo $datos[$contador]["Direccion"]; ?></td>
				            <td><?php echo $datos[$contador]["Nit"]; ?></td>
				        </tr>
				        <?php } ?>
				    </tbody>
				</table>
	        </div>
		</div>
		<img src="img/ipad.png" class="img-fluid" alt="">
	</div> 
  </section>

</body>
</html>