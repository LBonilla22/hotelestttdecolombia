<?php 
/* Listar Usuarios */
$datos = json_decode(file_get_contents("http://hotelesttt.webcindario.com/rest/ApiUsuarios.php"),true);
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body >
  <section id="signup" class="projects-section" style="background: url('img/bg-signup.jpg') no-repeat center center">
     <div class="container">

      <div class="row align-items-center no-gutters mb-4 mb-lg-5">
        <div class="col-xl-8 col-lg-7">
          <table class="table table-responsive text-white">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>Documento</th>
                    <th>Teléfono</th>
                    <th>Dirección</th>
                    <th>Correo electrónico</th>
                </tr>
            </thead>
            <tbody>
              <?php for ($contador=0; $contador < count($datos) ; $contador++) { ?>
                <tr>
                    <td><?php echo $datos[$contador]["Nombre"]; ?></td>
                    <td><?php echo $datos[$contador]["Apellido"]; ?></td>
                    <td><?php echo $datos[$contador]["Documento"]; ?></td>
                    <td><?php echo $datos[$contador]["Telefono"]; ?></td>
                    <td><?php echo $datos[$contador]["Direccion"]; ?></td>
                    <td><?php echo $datos[$contador]["Correo"]; ?></td>
                </tr>
                <?php } ?>
            </tbody>
          </table>
        </div>
        <div class="col-xl-4 col-lg-5" >
          <div class="featured-text text-center text-lg-left">
            <h4 style="color: white;">Gestión de Usuarios</h4>
            <p class="text-black-50 mb-0">
                <form class="form-inline d-flex" method="post" >
                <input type="text" class="form-control flex-fill mr-0 mr-sm-2 mb-3 mb-sm-0" name="Nombre" placeholder="Nombre" required="">
                <input type="text" class="form-control flex-fill mr-0 mr-sm-2 mb-3 mb-sm-0" name="Apellido" placeholder="Apellido" required="">
                <input type="text" class="form-control flex-fill mr-0 mr-sm-2 mb-3 mb-sm-0" name="Documento" placeholder="Documento" required="">
                <input type="text" class="form-control flex-fill mr-0 mr-sm-2 mb-3 mb-sm-0" name="Telefono" placeholder="Teléfono" required="">
                <input type="text" class="form-control flex-fill mr-0 mr-sm-2 mb-3 mb-sm-0" name="Direccion" placeholder="Dirección" required="">
                <input type="text" class="form-control flex-fill mr-0 mr-sm-2 mb-3 mb-sm-0" name="Correo" placeholder="Correo electrónico" required="">  
                <input type="password" class="form-control flex-fill mr-0 mr-sm-2 mb-3 mb-sm-0" name="Clave" placeholder="Contraseña" required="">    
                <input type="password" class="form-control flex-fill mr-0 mr-sm-2 mb-3 mb-sm-0" name="Confirmacion" placeholder="Confirme contraseña" required=""> 
                <input type="hidden" name="UsuarioSesion" value="<?php echo $session['Usuario']; ?>"> 
                <input type="hidden" name="NombreSesion" value="<?php echo $session['Nombre']; ?>">           
                <input type="submit" name="UsuarioGuardar" class=" btn-primary mx-auto btn-xs" value="Guardar">
            </form>
            </p>
          </div>
        </div>
      </div>

    </div>
  </section>
</body>
</html>