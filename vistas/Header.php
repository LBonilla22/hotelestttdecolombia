<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
  <header class="masthead">
    <div class="container d-flex h-100 align-items-center">
      <div class="mx-auto text-center">
        <h1 class="mx-auto my-0 text-uppercase">Hoteles TTT De Colombia</h1>
        <?php 
        if (!isset($session['Usuario'])) { ?>
	        <h2 class="text-white-50 mx-auto mt-2 mb-5">Ingrese sus credenciales para iniciar</h2>
	         <form class="form-inline d-flex" method="post">
	        <input type="text" class="form-control flex-fill mr-0 mr-sm-2 mb-3 mb-sm-0" name="Usuario" placeholder="Documento" required="">
	        <input type="password" class="form-control flex-fill mr-0 mr-sm-2 mb-3 mb-sm-0" name="Clave" placeholder="Contraseña" required="">
	        <input type="submit" name="Login" class="btn btn-primary mx-auto" value="Inicio">
    	<?php }else{ ?>
    		<h2 class="text-white-50 mx-auto mt-2 mb-5">¡Bienvenido <?php echo $session['Nombre']; ?>!</h2>
        	<a href="#about" class="btn btn-primary js-scroll-trigger">Iniciar</a>
        <?php } ?>
        </form>
      </div>
    </div>
  </header>
</body>
</html>