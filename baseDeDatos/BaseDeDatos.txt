create database HotelesTTTDeColombia;

use HotelesTTTDeColombia;

create table Hoteles(IdHotel bigint not null auto_increment,Nombre varchar(100) not null, Ciudad varchar(50) not null, NumeroHabitaciones varchar(50) not null, Direccion varchar(50) not null, Nit varchar(50) not null, primary key(IdHotel));
create table Habitaciones(IdHabitacion bigint not null auto_increment, IdHotel bigint not null, Cantidad bigint not null, TipoHabitacion varchar(50) not null, Acomodacion varchar(50) not null,primary key(IdHabitacion), foreign key(IdHotel) references Hoteles(IdHotel) on update cascade);
create table Usuarios(Nombre varchar(50) not null,Apellido varchar(50) not null, Documento varchar(20) not null,Telefono varchar(15) not null, Direccion varchar(50) not null,Correo varchar(50) not null,primary key(Documento));
create table Login(Tipo varchar(50) not null,Usuario varchar(100) not null,Clave varchar(100) not null,primary key(usuario)); 



/* trigger */

create trigger eliminar_login_bd before delete on Usuarios for each row delete from Login where(usuario = old.documento);


/* Insert de prueba */

insert into usuarios values('Carlos','Ramirez','1010','3173489155','cra 75 No. 23-12','CarlosRamirez@gmail.com');
insert into login values('Administrador','1010','$2y$12$KWdWJpdQYW3Kz/EF3YCc5ek15/5QF27B1Gh7nII2FxvXWXkxy3edG');

/*La clave es 1234*/


