<?php 
require_once('../modelos/Logica.php');
$logica = new Logicas();

if (isset($_GET['Nit']) AND isset($_GET["Nombre"]) AND isset($_GET["Ciudad"]) AND isset($_GET["Direccion"]) AND isset($_GET["NumeroHabitaciones"])) {
	$nit = str_replace("|.|", " ",$_GET['Nit']);
	if (count($logica->HotelBuscar($nit)) > 0) {
		echo json_encode(array('Mensaje'=>'Este hotel ya existe'));		
	}else{

		echo json_encode(array('Mensaje'=>$logica->HotelGuardar(str_replace("|.|", " ",$_GET["Nombre"]),str_replace("|.|", " ",$_GET["Ciudad"]),str_replace("|.|", " ",$_GET["NumeroHabitaciones"]),str_replace("|.|", " ",$_GET["Direccion"]),str_replace("|.|", " ",$_GET["Nit"]))));
	}
	
}else{

	$datos = $logica->HotelBuscarListar();	
		echo json_encode($datos); 
}

?>