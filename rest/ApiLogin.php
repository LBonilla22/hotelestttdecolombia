<?php 
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
	
	require_once('../modelos/Logica.php');
	$logica = new Logicas();

	$usuario = strip_tags($_GET['usuario']);
	$clave = strip_tags($_GET['clave']);
	
	$inicio = $logica->Usuario($usuario);

	if (isset($inicio[0]["Usuario"])) {

		if (password_verify($clave,$inicio[0]['Clave'])) {

			if ($inicio['0']['Tipo']=="Administrador") {	
				echo json_encode(				 array(
					'Documento'=> $inicio['0']['Documento'],
					'Nombre'=> $inicio['0']['Nombre'],
					'Apellido'=>$inicio['0']['Apellido']
				));
			}

		}else{

			echo json_encode(array('Mensaje'=>'Usuario o clave incorrecta'));
		}

	}else{

		echo json_encode(array('Mensaje'=>'Usuario o clave incorrecta'));

	}
}else
{
	echo json_encode(array('Mensaje'=>'Usuario o clave incorrecta'));
}

?>
