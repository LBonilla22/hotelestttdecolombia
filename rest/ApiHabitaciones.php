<?php 

require_once('../modelos/Logica.php');
$logica = new Logicas();
$arreglo = array();
$numeroHabitaciones = array();
$resultado = 0;

if (isset($_GET["IdHotel"]) AND isset($_GET["Cantidad"]) AND isset($_GET["TipoHabitacion"]) AND isset($_GET["Acomodacion"])) {

	$arreglo = $logica->HabitacionBuscarHotel(str_replace("|.|", " ",$_GET['IdHotel']));
	for ($contador=0; $contador < count($arreglo); $contador++) { 
		$resultado = $resultado + $arreglo[$contador]["Cantidad"];
	}
	$numeroHabitaciones = $logica->HotelBuscarId(str_replace("|.|", " ",$_GET['IdHotel']));
	if (($resultado + str_replace("|.|", " ",$_GET['Cantidad'])) > $numeroHabitaciones[0]["NumeroHabitaciones"]) {
		echo json_encode(array('Mensaje'=>'Supero la cantidad de habitaciones del hotel elegido'));		
	}else
	{		
		if (($_GET['Acomodacion'] == "Sencilla" AND ($_GET['TipoHabitacion'] == "Estándar" OR $_GET['TipoHabitacion'] == "Suite")) OR ($_GET['Acomodacion'] == "Doble" AND ($_GET['TipoHabitacion'] == "Estándar" OR $_GET['TipoHabitacion'] == "Suite")) OR ($_GET['Acomodacion'] == "Triple" AND ($_GET['TipoHabitacion'] == "Junior" OR $_GET['TipoHabitacion'] == "Suite")) OR ($_GET['Acomodacion'] == "Cuádruple" AND $_GET['TipoHabitacion'] == "Junior")) 
		{
			$numero = $logica->HabitacionBuscar(str_replace("|.|", " ",$_GET["IdHotel"]),str_replace("|.|", " ",$_GET["Cantidad"]),str_replace("|.|", " ",$_GET["TipoHabitacion"]),str_replace("|.|", " ",$_GET["Acomodacion"]));

			if (count($numero) > 0) {
				echo json_encode(array('Mensaje'=>$logica->HabitacionActualizar(str_replace("|.|", " ",$_GET["IdHotel"]),str_replace("|.|", " ",$_GET["Cantidad"]),str_replace("|.|", " ",$_GET["TipoHabitacion"]),str_replace("|.|", " ",$_GET["Acomodacion"]))));		
			}else{

				echo json_encode(array('Mensaje'=>$logica->HabitacionGuardar(str_replace("|.|", " ",$_GET["IdHotel"]),str_replace("|.|", " ",$_GET["Cantidad"]),str_replace("|.|", " ",$_GET["TipoHabitacion"]),str_replace("|.|", " ",$_GET["Acomodacion"]))));
		    }
		}else{
			echo json_encode(array('Mensaje'=>'Esta acomodacion no es compatible con el tipo de habitacion'));
		}
	}
}else{

	$datos = $logica->HabitacionesBuscarListar();	
		echo json_encode($datos);
}

?>