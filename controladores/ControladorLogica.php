<?php 
/* Login */
if(isset($_POST['Login']))
{
  $datos = json_decode(file_get_contents("http://hotelesttt.webcindario.com/rest/ApiLogin.php?usuario=".$_POST['Usuario']."&clave=".$_POST['Clave']),true);

  if(isset($datos['Mensaje']))
  {
    echo "<script>alert('".$datos['Mensaje']."');</script>";
  }else{      
    $session['Usuario'] = $datos["Documento"];
    $session['Nombre'] = $datos['Nombre'] .' '. $datos['Apellido'];
  }
}

/* Hoteles */
if (isset($_POST['HotelGuardar'])) {

	$datos = json_decode(file_get_contents("http://hotelesttt.webcindario.com/rest/ApiHoteles.php?Nombre=".str_replace(" ", "|.|", $_POST["Nombre"])."&Ciudad=".str_replace(" ", "|.|",$_POST["Ciudad"])."&NumeroHabitaciones=".str_replace(" ", "|.|",$_POST["Numero"])."&Direccion=".str_replace(" ", "|.|",$_POST["Direccion"])."&Nit=".str_replace(" ", "|.|",$_POST["Nit"])),true);
	echo "<script>alert('".$datos['Mensaje']."');location='#about';</script>";
  $session['Usuario'] = $_POST['UsuarioSesion'];
  $session['Nombre'] = $_POST['NombreSesion'];
}

/* Habitaciones */
if (isset($_POST['HabitacionesGuardar'])) {

	$datos = json_decode(file_get_contents("http://hotelesttt.webcindario.com/rest/ApiHabitaciones.php?IdHotel=".str_replace(" ", "|.|",$_POST["IdHotel"])."&Cantidad=".str_replace(" ", "|.|",$_POST["Cantidad"])."&TipoHabitacion=".str_replace(" ", "|.|",$_POST["TipoHabitacion"])."&Acomodacion=".str_replace(" ", "|.|",$_POST["Acomodacion"])),true);
	echo "<script>alert('".$datos['Mensaje']."');location='#projects';</script>";
  $session['Usuario'] = $_POST['UsuarioSesion'];
  $session['Nombre'] = $_POST['NombreSesion'];
}

/* Usuarios */
if (isset($_POST['UsuarioGuardar'])) {
  if (str_replace(" ", "|.|",$_POST['Confirmacion'])==str_replace(" ", "|.|",$_POST['Clave'])) {
    $datos = json_decode(file_get_contents("http://hotelesttt.webcindario.com/rest/ApiUsuarios.php?Nombre=".str_replace(" ", "|.|",$_POST["Nombre"])."&Apellido=".str_replace(" ", "|.|",$_POST["Apellido"])."&Documento=".str_replace(" ", "|.|",$_POST["Documento"])."&Telefono=".str_replace(" ", "|.|",$_POST["Telefono"])."&Direccion=".str_replace(" ", "|.|",$_POST["Direccion"])."&Correo=".str_replace(" ", "|.|",$_POST["Correo"])."&Clave=".$_POST["Clave"]),true);
    echo "<script>alert('".$datos['Mensaje']."');location='#signup';</script>";
    $session['Usuario'] = $_POST['UsuarioSesion'];
    $session['Nombre'] = $_POST['NombreSesion'];
  }else{
    echo "<script>alert('La confirmación no corresponde a la contraseña');</script>";
  }
}

/* Destruir sesiones */
if (isset($_GET['Salir'])) {
  session_destroy();
}

?>