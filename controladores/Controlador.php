<?php 
  require_once("controladores/ControladorLogica.php");
 ?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Hoteles TTT De Colombia</title>

  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <link href="css/grayscale.min.css" rel="stylesheet">

</head>

<body id="page-top">

<?php  

/* Header */
include_once("vistas/Header.php");

if (isset($session['Usuario'])) {

  /* Menu */
  include_once('vistas/Menu.php'); 

  /* Hoteles */
   include_once('vistas/Hoteles.php'); 

  /* Habitaciones */
  include_once('vistas/Habitaciones.php');

  /* Perfil */
   include_once('vistas/Perfiles.php'); 

  /* Contactenos */
   include_once('vistas/Contactenos.php'); 

}
 /* Footer */
// include_once('vistas/Footer.php'); 
 ?>
</body>

</html>





