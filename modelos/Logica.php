<?php 
require_once("Conexion.php");

class Logicas
{
	/*Constructro del formulario*/
	public function Logica()
	{
		$this->arreglo = array();
		$this->mensaje = "";
	}

	/* Busca el usuario */
	public function Usuario($usuario)
	{
		try 
		{
			$sql = "SELECT * FROM Login inner join Usuarios on Documento = Usuario WHERE Usuario = '$usuario' ";
			$sq = mysqli_query(Conexion::conectar(),$sql);
			$mySql = mysqli_fetch_assoc($sq);
			$this->arreglo[] = $mySql;				
		} catch (Exception $e) {
			$this->arreglo[] = $e;
		}
		return $this->arreglo;
	}

	/* Guardar Hotel */
	public function HotelGuardar($nombre,$ciudad,$numeroHabitaciones,$direccion,$nit)
	{		
		try {
			$sql = "INSERT INTO Hoteles VALUES(NULL,:Nombre,:Ciudad,:NumeroHabitaciones,:Direccion,:nit)";
			$sq = Conexion::conexionPDO()->prepare($sql);
			$sq->execute(array(":Nombre"=>$nombre,":Ciudad"=>$ciudad,":NumeroHabitaciones"=>$numeroHabitaciones,":Direccion"=>$direccion,":nit"=>$nit));

			$this->mensaje = "Datos guardados satisfactoriamente";
			
		} catch (Exception $e) {
			$this->mensaje = "Error: ".$e;
		}
		return $this->mensaje;
	}

	/* Buscar Hotel por nit */	
	public function HotelBuscar($nit)
	{
		try {
			$this->arreglo = array();
			$sql = "SELECT * FROM Hoteles WHERE Nit = :Nit";
			$sq = Conexion::conexionPDO()->prepare($sql);
			$sq -> execute(array(":Nit"=>$nit));

			while ($resultado = $sq->fetch(pdo::FETCH_ASSOC)) {
				$this->arreglo[] = $resultado;
			}
			return $this->arreglo;
		} catch (Exception $e) {
		}		
	}

	/* Buscar hotel por IdHotel */
	public function HotelBuscarId($idHotel)
	{
		try {
			$this->arreglo = array();
			$sql = "SELECT NumeroHabitaciones FROM Hoteles WHERE IdHotel = :IdHotel";
			$sq = Conexion::conexionPDO()->prepare($sql);
			$sq -> execute(array(":IdHotel"=>$idHotel));

			while ($resultado = $sq->fetch(pdo::FETCH_ASSOC)) {
				$this->arreglo[] = $resultado;
			}
			return $this->arreglo;
		} catch (Exception $e) {
		}		
	}

	/* Listar hoteles */
	public function HotelBuscarListar()
	{
		try {
			$this->arreglo = array();
			$sql = "SELECT * FROM Hoteles";
			$sq = Conexion::conexionPDO()->prepare($sql);
			$sq -> execute(array());

			while ($resultado = $sq->fetch(pdo::FETCH_ASSOC)) {
				$this->arreglo[] = $resultado;
			}
			return $this->arreglo;
		} catch (Exception $e) {
		}		
	}

	/* Guardar habitación */
	public function HabitacionGuardar($idHotel,$cantidad,$tipoHabitacion,$acomodacion)
	{
		try {
			$sql = "INSERT INTO Habitaciones VALUES (NULL,:IdHotel,:Cantidad,:TipoHabitacion,:Acomodacion)";
			$sq = Conexion::conexionPDO()->prepare($sql);
			$sq->execute(array(":IdHotel"=>$idHotel,":Cantidad"=>$cantidad,":TipoHabitacion"=>$tipoHabitacion,":Acomodacion"=>$acomodacion));

			$this->mensaje = "Datos guardados satisfactoriamente";
			
		} catch (Exception $e) {
			$this->mensaje = "Error: ".$e;
		}
		return $this->mensaje;
	}

	/* Actualizar habitación */
	public function HabitacionActualizar($idHotel,$cantidad,$tipoHabitacion,$acomodacion)
	{
		try {
			$sql = "UPDATE Habitaciones SET Cantidad= Cantidad + :Cantidad WHERE TipoHabitacion = :TipoHabitacion AND 
			Acomodacion = :Acomodacion AND IdHotel = :IdHotel";
			$sq = Conexion::conexionPDO()->prepare($sql);
			$sq->execute(array(":IdHotel"=>$idHotel,":Cantidad"=>$cantidad,":TipoHabitacion"=>$tipoHabitacion,":Acomodacion"=>$acomodacion));

			$this->mensaje = "Datos guardados satisfactoriamente";
			
		} catch (Exception $e) {
			$this->mensaje = "Error: ".$e;
		}
		return $this->mensaje;
	}

	/* Buscar tipo Habitacion */	
	public function HabitacionBuscar($idHotel,$cantidad,$tipoHabitacion,$acomodacion)
	{
		try {
			$this->arreglo = array();
			$sql = "SELECT * FROM Habitaciones WHERE TipoHabitacion = :TipoHabitacion AND Acomodacion = :Acomodacion AND IdHotel = :IdHotel";
			$sq = Conexion::conexionPDO()->prepare($sql);
			$sq -> execute(array(":IdHotel"=>$idHotel,":TipoHabitacion"=>$tipoHabitacion,":Acomodacion"=>$acomodacion));

			while ($resultado = $sq->fetch(pdo::FETCH_ASSOC)) {
				$this->arreglo[] = $resultado;
			}
			return $this->arreglo;
		} catch (Exception $e) {
		}		
	}

	/* Buscar Habitacion por hotel */	
	public function HabitacionBuscarHotel($idHotel)
	{
		try {
			$this->arreglo = array();
			$sql = "SELECT Cantidad FROM Habitaciones WHERE IdHotel = :IdHotel";
			$sq = Conexion::conexionPDO()->prepare($sql);
			$sq -> execute(array(":IdHotel"=>$idHotel));

			while ($resultado = $sq->fetch(pdo::FETCH_ASSOC)) {
				$this->arreglo[] = $resultado;
			}
			return $this->arreglo;
		} catch (Exception $e) {
		}		
	}

	/* Listar Habitaciones */
	public function HabitacionesBuscarListar()
	{
		try {
			$this->arreglo = array();
			$sql = "SELECT Hoteles.Nombre, Hoteles.NumeroHabitaciones,Habitaciones.Cantidad,Habitaciones.TipoHabitacion,habitaciones.Acomodacion
				FROM Habitaciones
				INNER JOIN Hoteles ON Hoteles.IdHotel = Habitaciones.IdHotel
				ORDER BY Hoteles.Nombre DESC";
			$sq = Conexion::conexionPDO()->prepare($sql);
			$sq -> execute(array());

			while ($resultado = $sq->fetch(pdo::FETCH_ASSOC)) {
				$this->arreglo[] = $resultado;
			}
			return $this->arreglo;
		} catch (Exception $e) {
		}		
	}

	/* Guardar usuario */
	public function UsuarioGuardar($nombre,$apellido,$documento,$telefono,$direccion,$correo,$clave)
	{
		try {
			$sql = "INSERT INTO Usuarios VALUES(:Nombre,:Apellido,:Documento,:Telefono,:Direccion,:Correo)";
			$sq = Conexion::conexionPDO()->prepare($sql);
			$sq->execute(array(":Nombre"=>$nombre,":Apellido"=>$apellido,":Documento"=>$documento,":Telefono"=>$telefono,":Direccion"=>$direccion,":Correo"=>$correo));

			$pass=password_hash($clave,PASSWORD_DEFAULT, array("cost"=>12));
			$sql = "INSERT INTO Login VALUES('Administrador',:Usuario,:Clave)";
			$sq = Conexion::conexionPDO()->prepare($sql);
			$sq->execute(array(":Usuario"=>$documento,":Clave"=>$pass));

			$this->mensaje = "Datos guardados satisfactoriamente";
			
		} catch (Exception $e) {
			$this->mensaje = "Error: ".$e;
		}
		return $this->mensaje;
	}

	/* Listar Usuarios */
	public function UsuariosBuscarListar()
	{
		try {
			$this->arreglo = array();
			$sql = "SELECT * FROM Usuarios";
			$sq = Conexion::conexionPDO()->prepare($sql);
			$sq -> execute(array());

			while ($resultado = $sq->fetch(pdo::FETCH_ASSOC)) {
				$this->arreglo[] = $resultado;
			}
			return $this->arreglo;
		} catch (Exception $e) {
		}		
	}

	/* Buscar por documento */	
	public function UsuarioBuscar($documento)
	{
		try {
			$this->arreglo = array();
			$sql = "SELECT * FROM Usuarios WHERE Documento = :Documento";
			$sq = Conexion::conexionPDO()->prepare($sql);
			$sq -> execute(array(":Documento"=>$documento));

			while ($resultado = $sq->fetch(pdo::FETCH_ASSOC)) {
				$this->arreglo[] = $resultado;
			}
			return $this->arreglo;
		} catch (Exception $e) {
		}		
	}

}


?>